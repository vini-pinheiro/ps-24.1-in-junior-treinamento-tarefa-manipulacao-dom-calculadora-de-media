let grades = [];
let index = 0;
document.getElementById("addGrade").addEventListener("click", displayGrade);
document.getElementById("calculateAverage").addEventListener("click", displayAverage);

//mostra a mensagem de erro
function errorMessage(text) {
    let grade = text.replace(",", ".");
    if(grade == '') alert('Por favor, insira uma nota.');
    else if(isNaN(grade)) alert('A nota digitada é inválida, por favor, insira uma nota válida.');
    else 
    {
        grade = parseFloat(grade);
        if(grade>10 || grade<0) 
        {
            alert('A nota digitada é inválida, por favor, insira uma nota válida.')
        }
        else
        {
            return false;
        }
    }
    return true;
}

//verifica a nota
function verifyGrade(grade)
{
    if(errorMessage(grade)) return false;
    else return true;
}

//mostra dentro do html
function displayGrade() {
    let stringGrade = document.getElementById("grade").value;
    if(!verifyGrade(stringGrade)) return null;
    let numberGrade = parseFloat(stringGrade.replace(",", "."));
    grades.push(numberGrade);
    console.log(grades);
    document.getElementById("output-grades").innerHTML += `Nota ${index+1}: ${grades[index]}\n`;
    index++;
}

//mostra a média dentro do html
function displayAverage() {
    let average = 0;
    for (const index in grades) {
        average += grades[index];
        console.log(average);
    }
    average = average / grades.length;
    document.getElementById("output-result").innerHTML = average.toFixed(2);
}